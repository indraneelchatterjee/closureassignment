let limitFunctionCallCount = (cb, n) => {
  if (typeof cb !== "function" || typeof n !== "number" || n <= 0) {
    const myErr = new Error("The arguments were not valid.");
    throw myErr.message;
  }
  let count = n;
  return function (...num) {
    if (count > 0) {
      count--;
      return cb(...num);
    } else {
      console.log(null);
      return null;
    }
  };
};

module.exports = limitFunctionCallCount;
