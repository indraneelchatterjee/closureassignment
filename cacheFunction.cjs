let cacheFunction = (cb) => {
  if (typeof cb !== "function") {
    throw new Error("Invalid Argument");
  }
  let cache = {};

  return function (...val) {
    let strVal = JSON.stringify(val);
    if (!cache[strVal]) {
      cache[strVal] = cb(...val);
      return cache[strVal];
    } else {
      console.log("from cache", cache[strVal]);
      return cache[strVal];
    }
  };
};

module.exports = cacheFunction;
