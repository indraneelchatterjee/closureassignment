let cacheFunction = require("../cacheFunction.cjs");
let cb = (a, b, c) => {
  try {
    let ans = Math.min(a, b);
    console.log(ans);
    return a;
  } catch (error) {
    console.log(error);
  }
};

let cache = cacheFunction(cb);
cache(2, 3);
// cache(3);
// cache(4);
// cache([2, 1, 3]);
// cache(2);
// cache(3);
