const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

let cb = (num) => {
  try {
    console.log(`cb was called ${num}`);
  } catch (error) {
    console.log(error);
  }
};

let limitcall = limitFunctionCallCount(cb, 2);

limitcall(2);
limitcall(4);
limitcall(5);
limitcall(6);
